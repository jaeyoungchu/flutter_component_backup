import 'package:sqflite/sqflite.dart';
import 'dart:async';
import 'dart:io' as io;
import 'package:path_provider/path_provider.dart';
import 'package:path/path.dart';
import 'package:ui_backup/model/Contact.dart';

class DBHelper{
  static Database db_instance;
  final String TABLE_NAME = "Contact";
  final String DB_NAME = "EDMTDev.db";
  String query;

  Future<Database> get db async {
    print('get db!!!!!!');
    if(db_instance == null){
      print('get db is null  !!!!!!');
      db_instance = await initDB();
    }
    print('get db is not null  !!!!!!');
    return db_instance;
  }

  initDB() async{
    io.Directory documentsDirectory = await getApplicationDocumentsDirectory();
    print('init db !!!!!!');
    String path = join(documentsDirectory.path,DB_NAME);
    var db = await openDatabase(path,version: 3,onCreate: onCreateFunc);
    await db.execute('CREATE TABLE IF NOT EXISTS $TABLE_NAME (id INTEGER PRIMARY KEY AUTOINCREMENT, name TEXT, phone TEXT);');
    return db;
  }

  void onCreateFunc(Database db, int version) async{
    print('on create!!!!!!');
    await db.execute('CREATE TABLE $TABLE_NAME (id INTEGER PRIMARY KEY AUTOINCREMENT, name TEXT, phone TEXT);');
  }

  Future<List<Contact>> getContacts() async{
    var db_connection = await db;
    List<Map> list = await db_connection.rawQuery('SELECT * FROM $TABLE_NAME');
    List<Contact> contacts = new List();
    Contact contact;
    for(int i = 0; i < list.length;i++){
      contact = new Contact(list[i]['id'], list[i]['name'], list[i]['phone']);
      contacts.add(contact);
    }
    return contacts;
  }

  Future<Contact> getContact(int id) async{
    var db_connection = await db;
    var result = await db_connection.rawQuery('SELECT * FROM $TABLE_NAME WHERE id=$id');

    if (result.length > 0) {
      Contact contact = Contact.fromMap(result.first);
      print(contact.name);
      return contact;
    }

    return null;
  }

  void addNewContact(Contact contact) async{
    var db_connection = await db;
    query = 'INSERT INTO $TABLE_NAME(name,phone) VALUES(\'${contact.name}\',\'${contact.phone}\')';
    await db_connection.transaction((transaction) async {
      return await transaction.rawInsert(query);
    });
  }

  void updateContact(Contact contact) async {
    var db_connection = await db;
    query = 'UPDATE $TABLE_NAME SET name=\'${contact.name}\',phone=\'${contact.phone}\' WHERE id=${contact.id}';
    await db_connection.transaction((transaction) async{
      return await transaction.rawQuery(query);
    });
  }

  void deleteContact(Contact contact) async{
    var db_connection = await db;
    query = 'DELETE FROM $TABLE_NAME WHERE id=${contact.id}';
    await db_connection.transaction((transaction) async{
      return await transaction.rawQuery(query);
    });
  }
  void resetDB() async {
    var db_connection = await db;
    query = 'DROP TABLE IF EXISTS ${TABLE_NAME}';
    await db_connection.transaction((transaction) async{
      await transaction.rawQuery(query);
    });
    print('pre');
    db_connection.execute('CREATE TABLE $TABLE_NAME (id INTEGER PRIMARY KEY AUTOINCREMENT, name TEXT, phone TEXT);');
    print('after');
  }
}




















