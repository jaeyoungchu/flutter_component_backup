import 'package:flutter/material.dart';

class Home extends StatelessWidget {
  final List<String> screenNames = new List();

  Home() {
    screenNames.add("chuTextInputEx");
    screenNames.add("expansionTileEx");
    screenNames.add("futureBuilderEx");
    screenNames.add("noFutureBuilderEx");
    screenNames.add("chuButtonEx");
    screenNames.add("progressButtonEx");
    screenNames.add("backdropEx");
    screenNames.add("dialogEx");
    screenNames.add("multiListviewEx");
    screenNames.add("sqfliteEx");
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: ListView.builder(
        itemBuilder: (BuildContext context, int index) {
          return listItems(context, index);
        },
        itemCount: screenNames.length,
      ),
    );
  }

  Widget listItems(BuildContext context,int index) {
    return GestureDetector(
      onTap: (){moveToScreen(context,index);},
      child: Card(
          child: Padding(
        padding: const EdgeInsets.all(12.0),
        child: Center(
            child: Text('${screenNames[index]}',
          style: TextStyle(fontSize: 20.0),
        )),
      )),
    );
  }

  moveToScreen(BuildContext context,int index){
    Navigator.of(context).pushNamed('/${screenNames[index]}');
  }

}
