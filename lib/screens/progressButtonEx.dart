import 'dart:async';

import 'package:flutter/material.dart';

class ProgressButtonEx extends StatefulWidget {
  @override
  _ProgressButtonExState createState() => _ProgressButtonExState();
}

enum status {
  idle,
  progressing,
  done
}

class _ProgressButtonExState extends State<ProgressButtonEx> with TickerProviderStateMixin{

  status currentStatus = status.idle;
  double width = double.infinity;
  Animation _animation;
  GlobalKey globalKey = GlobalKey();
  double beginAnimation=0.0,endAnimation=1.0;
  double initialWidth;
  var controller;
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(
        child: Container(
          key: globalKey,
          height: 48.0,
          width: width,
          child: IgnorePointer(
            ignoring: (currentStatus == status.progressing),
            child: RaisedButton(
              padding: EdgeInsets.all(0.0),
                onPressed: (){animateButton();},
              child: buildButtonChild(),
              shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(25.0)),
              color: currentStatus == status.done ? Colors.green:Colors.blue,
                ),
          ),
        ),
      ),
    );
  }
  void animateButton(){

    if(currentStatus == status.idle){

      double currentWidth = globalKey.currentContext.size.width;
      initialWidth = currentWidth;
      controller = AnimationController(duration: Duration(milliseconds: 200),vsync: this);

      _animation = Tween(begin: 0.0,end: 1.0).animate(controller)..addListener((){
        setState(() {
          width = currentWidth - ((currentWidth - 48.0) * _animation.value);
        });
      });

      controller.forward();
      setState(() {
        currentStatus = status.progressing;
      });

      Timer(Duration(milliseconds:1500), (){
        setState(() {
          currentStatus = status.done;
        });
      });
    }else{

      double currentWidth = initialWidth;
      print(currentWidth);

      controller = AnimationController(duration: Duration(milliseconds: 200),vsync: this);

      _animation = Tween(begin: 0.0,end: 1.0).animate(controller)..addListener((){
        setState(() {
          width = currentWidth * _animation.value;
        });
      });

      controller.forward();
      setState(() {
        currentStatus = status.progressing;
      });


      Timer(Duration(milliseconds:500), (){
        setState(() {
          currentStatus = status.idle;
        });
      });
    }
  }


  @override
  void dispose() {
    controller.dispose();
    super.dispose();
  }

  Widget buildButtonChild(){
    if(currentStatus == status.idle){
      return Text('Login',style: TextStyle(color: Colors.white),);
    }else if(currentStatus == status.progressing){
      return CircularProgressIndicator(
        value: null,
        valueColor: AlwaysStoppedAnimation<Color>(Colors.white),
      );
    }else{
      return Icon(Icons.check,color: Colors.white,size: 48,);
    }
  }
}
