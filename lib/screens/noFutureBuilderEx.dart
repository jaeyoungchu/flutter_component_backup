import 'dart:convert';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;

class NoFutureBuilderEx extends StatefulWidget {
  @override
  _NoFutureBuilderExState createState() => _NoFutureBuilderExState();
}

class _NoFutureBuilderExState extends State<NoFutureBuilderEx> {
  List<User> users;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    _getUser();
  }

  @override
  Widget build(BuildContext context) {
    if(users == null){
      return Scaffold(
        body: Center(child: CircularProgressIndicator(backgroundColor: Colors.orange,strokeWidth: 5.0,)),
      );
    }
    return Scaffold(
      body: ListView.builder(
        itemBuilder: (BuildContext context, int index) {
          return ListTile(
            key: PageStorageKey<User>(users[index]),
            leading: CircleAvatar(
              backgroundColor: Colors.white,
                    backgroundImage: NetworkImage(_cutUri(users[index].picture.toString())),//snapshot.data[index].picture.toString()
//              backgroundImage: NetworkImage(_cutUri(
//                  "https://robohash.org/quibusdampraesentiumaccusantium.png")),
            ),
            title: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Text(
                  users[index].name,
                  style: TextStyle(fontSize: 25.0),
                ),
                Text(
                  "<${users[index].title}>",
                  style: TextStyle(fontSize: 12.0),
                ),
              ],
            ),
            subtitle: Text(users[index].email),
            contentPadding: EdgeInsets.all(12.0),
          );
        },
        itemCount: users.length,
      ),
    );
  }

  String _cutUri(String uri) {
    if (uri == null ||uri.isEmpty ) {
      return "https://robohash.org/quibusdampraesentiumaccusantium.png";
    }
    return uri.substring(0,uri.indexOf('?'));
  }

  void _getUser() async {
    var data = await http
        .get("https://api.mockaroo.com/api/df728080?count=10&key=726cf6b0");
    var jsonData = await json.decode(data.body);

    List<User> users = [];
    User user;
    for (var u in jsonData) {
      user = User(u["id"], u["title"], u["name"], u["email"], u["picture"]);
      users.add(user);
    }
    setState(() {
      this.users = users;
    });
  }


}

class User {
  final int id;
  final String title, name, email, picture;

  User(this.id, this.title, this.name, this.email, this.picture);
}
