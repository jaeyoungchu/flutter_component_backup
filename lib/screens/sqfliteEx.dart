import 'package:flutter/material.dart';
import 'dart:async';
import 'package:ui_backup/components/chuButton.dart';
import 'package:ui_backup/model/Contact.dart';
import 'package:ui_backup/utils/dbHelper.dart';
import 'package:fluttertoast/fluttertoast.dart';

class SqfliteEx extends StatefulWidget {
  @override
  _SqfliteExState createState() => _SqfliteExState();
}

class _SqfliteExState extends State<SqfliteEx> {

  String dbDetails="";

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          Text(dbDetails),
          SizedBox(height: 10.0,),
          ChuButton(addContact,"AddContact"),
          SizedBox(height: 10.0,),
          ChuButton(updateContact,"UpdateContact"),
          SizedBox(height: 10.0,),
          ChuButton(getContactOne,"GetContact"),
          SizedBox(height: 10.0,),
          ChuButton(deleteContact,"DeleteContact"),
          SizedBox(height: 10.0,),
          ChuButton(resetDB,"ResetDB"),
          SizedBox(height: 10.0,),
          ChuButton(showDB, "Show db"),
          SizedBox(height: 10.0,),
        ],
      ),
    );
  }

  void showDB()async{
    List<Contact> contacts = new List();
    var dbHelper = DBHelper();
    contacts = await dbHelper.getContacts();
    String result="";
    for(var contact in contacts){
      result = '$result id = ${contact.id}, name = ${contact.name}, phone = ${contact.phone} \n';
    }
    setState(() {
      dbDetails = result;
    });
  }

  void updateContact()async{
    var dbHelper = DBHelper();
    Contact contact = new Contact(1, "jin", "1234124");
    dbHelper.updateContact(contact);
    showDB();
  }
  void getContactOne() async{
    var dbHelper = DBHelper();
    Contact contact = await dbHelper.getContact(1);
    if(contact != null){
      setState(() {
        dbDetails = 'id = ${contact.id}, name = ${contact.name}, phone = ${contact.phone} \n';
      });
    }else{
      dbDetails = 'DB does not match..';
    }
  }

  void addContact(){
    Fluttertoast.showToast(msg: 'adding',toastLength: Toast.LENGTH_SHORT,backgroundColor: Colors.orange,textColor: Colors.white,gravity: ToastGravity.BOTTOM);
    var dbHelper = DBHelper();
    Contact contact = new Contact(-1, "chu", "641-487-5446");
    dbHelper.addNewContact(contact);
    showDB();
  }
  void deleteContact(){
    var dbHelper = DBHelper();
    Contact contact = new Contact(1, "chu", "641-487-5446");
    dbHelper.deleteContact(contact);
    showDB();
  }
  void resetDB(){
    var dbHelper = DBHelper();
    dbHelper.resetDB();
  }

}
