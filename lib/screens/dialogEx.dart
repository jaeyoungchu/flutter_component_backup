import 'package:flutter/material.dart';
import 'package:ui_backup/components/chuButton.dart';
import 'package:ui_backup/components/normalDialog.dart';
import 'package:ui_backup/components/styledDialog.dart';


class DialogEx extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          ChuButton((){
            chuNormalDialog(context, "hit it!");
          },'normal'),
          SizedBox(
            height: 15.0,
          ),
          ChuButton((){
            chuStyledDialog(context,'Styled!');
          },'styled'),
        ],
      ),
    );
  }
}

