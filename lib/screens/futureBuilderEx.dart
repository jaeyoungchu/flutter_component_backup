import 'dart:convert';
import 'package:http/http.dart' as http;
import 'package:flutter/material.dart';


class FutureBuilderEx extends StatelessWidget {

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: FutureBuilder(
        future: _getUser(),
        builder: (BuildContext context, AsyncSnapshot snapshot) {
          if(snapshot.data == null){
            return Center(child: CircularProgressIndicator(backgroundColor: Colors.orange,strokeWidth: 5.0,));
          }else{
            return ListView.builder(
              itemBuilder: (BuildContext context, int index){
                return ListTile(
                  key: PageStorageKey<User>(snapshot.data[index]),
                  leading: CircleAvatar(
                    backgroundColor: Colors.white,
//                    backgroundImage: NetworkImage("https://robohash.org/quibusdampraesentiumaccusantium.png"),//snapshot.data[index].picture.toString()
                    backgroundImage: NetworkImage(snapshot.data[index].picture.toString()),//
                  ),
                  title: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      Text(snapshot.data[index].name,style: TextStyle(fontSize: 25.0),),
                      Text("<${snapshot.data[index].title}>",style: TextStyle(fontSize: 12.0),),
                    ],
                  ),
                  subtitle: Text(snapshot.data[index].email),
                  contentPadding: EdgeInsets.all(12.0),
                );
              },
              itemCount: snapshot.data.length,
            );
          }


        },
      ),
    );
  }
  Future<List<User>> _getUser() async {
    var data = await http
        .get("https://api.mockaroo.com/api/df728080?count=10&key=726cf6b0");
    var jsonData = json.decode(data.body);

    List<User> users = [];
    User user;
    for (var u in jsonData) {
      user = User(u["id"], u["title"], u["name"], u["email"], _cutUri(u["picture"]));
      users.add(user);
    }
    return users;
  }
  String _cutUri(String uri){
    if(uri == null || uri.isEmpty){
      return "https://robohash.org/quibusdampraesentiumaccusantium.png";
    }
    return uri.substring(0,uri.indexOf('?'));
  }
}

class User {
  final int id;
  final String title, name, email, picture;

  User(this.id, this.title, this.name, this.email, this.picture);
}
