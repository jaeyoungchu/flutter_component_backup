import 'package:flutter/material.dart';

class MultiListViewEx extends StatelessWidget {


  @override
  Widget build(BuildContext context) {
    final screenHeight = MediaQuery.of(context).size.height;
    final screenWidth = MediaQuery.of(context).size.width;
    final menu = getMenuObject();
    return Scaffold(
      body: Container(
        child: ListView.builder(
          itemCount: menu.categoryList.length,
            itemBuilder: (BuildContext context, int index){
              return Padding(
                padding: const EdgeInsets.all(8.0),
                child: Card(
                  elevation: 3.0,
                  child: Container(
                    height: screenHeight/3,
                    child: Column(
                      children: <Widget>[
                        Expanded(
                          flex: 1,
                          child: Container(
                            width: double.infinity,
                            color: Colors.orange,
                            child: Center(
                                child: Text(
                                  menu.categoryList[index].categoryName,
                                  style: TextStyle(fontSize: 18.0,color: Colors.white,fontWeight: FontWeight.bold),
                                )
                            ),
                          ),
                        ),
                        Expanded(
                          flex: 8,
                          child: ListView.builder(
                            scrollDirection: Axis.horizontal,
                            itemCount: 5,
                              itemBuilder: (BuildContext context, int index){
                                return Padding(
                                  padding: const EdgeInsets.all(8.0),
                                  child: Card(
                                    elevation: 1.0,
                                    child: Container(
                                      width: 150.0,
                                      child: Image.network('https://snappyliving.com/wp-content/uploads/2012/07/cranberry-coke.jpg'),
                                    ),
                                  ),
                                );
                              }
                          ),
                        )
                      ],
                    ),
                  ),
                ),
              );
            }
        ),
      ),
    );
  }
}


Menu getMenuObject(){
  List<Product> productList = new List();
  productList.add(Product('Coke', 1.1));
  productList.add(Product('Sprite', 1.1));
  productList.add(Product('Coffee', 2.3));

  Category beverage = new Category('beverage',productList);

  productList.clear();
  productList.add(Product('Fried Chicken', 26.99));
  productList.add(Product('Galic Chicken', 28.99));
  productList.add(Product('Soy Source Chicken', 27.99));

  Category main = new Category('Main', productList);

  productList.clear();
  productList.add(Product('Cheese Burger', 6.99));
  productList.add(Product('Double Burger', 8.99));
  productList.add(Product('Original Burger', 5.99));

  Category burger = new Category('Burger', productList);

  List<Category> categoryList = new List();
  categoryList.add(beverage);
  categoryList.add(main);
  categoryList.add(burger);

  return new Menu(categoryList);

}

class Menu{
  List<Category> categoryList = new List();

  Menu(this.categoryList);
}

class Category{
  String categoryName;
  List<Product> productList = new List();

  Category(this.categoryName, this.productList);

}

class Product{
  String productName;
  double price,tax,totalPrice;
  int quantity;

  Product(this.productName, this.price);

}