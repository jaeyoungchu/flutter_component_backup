import 'package:flutter/material.dart';
import 'package:ui_backup/components/backdropPanel.dart';

class BackDropEx extends StatefulWidget {
  @override
  _BackDropExState createState() => _BackDropExState();
}

class _BackDropExState extends State<BackDropEx>
    with SingleTickerProviderStateMixin {
  AnimationController controller;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    controller = AnimationController(
        vsync: this, duration: Duration(milliseconds: 400), value: 1.0);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("BackDrop",style: TextStyle(color: Colors.white),),
        leading: IconButton(
            icon: AnimatedIcon(
                icon: AnimatedIcons.close_menu, progress: controller.view,color: Colors.white,),
            onPressed: () {
              controller.fling(velocity:  isPanelVisible ? -1.0 : 1.0);
            }
            ),
      ),
      body: BackdropPanels(controller: controller),
    );
  }

  bool get isPanelVisible{
    final AnimationStatus status = controller.status;

    return status == AnimationStatus.completed || status == AnimationStatus.forward;
  }
}

