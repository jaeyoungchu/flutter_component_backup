import 'package:flutter/material.dart';

class ExpansionTileEx extends StatelessWidget {

  final List<MyTile> tiles = List();

  ExpansionTileEx(){
    _getMyTiles(tiles);
  }


  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: ListView.builder(
          itemBuilder: (BuildContext context, int index){
            return _getExpansionTile(tiles[index]);
          },
        itemCount: tiles.length,
      ),
    );
  }

  Widget _getExpansionTile(MyTile t){


    if(t.children.isEmpty) return ListTile(title: Text("${t.title}"),);

    return ExpansionTile(
      key: PageStorageKey<MyTile>(t),
      title: Text(t.title),
      children: t.children.map(_getExpansionTile).toList(),
    );
  }

  void _getMyTiles(List<MyTile> tiles){

    MyTile animals = MyTile('animals');
    MyTile dogs = MyTile('Dogs');
    MyTile cats = MyTile('Cats');
    MyTile birds = MyTile('Birds');
    dogs.children.add(MyTile('Chiwawa'));
    dogs.children.add(MyTile('Pug'));
    animals.children.add(dogs);
    animals.children.add(cats);
    animals.children.add(birds);

    tiles.add(animals);
    tiles.add(MyTile('Cars'));
  }
}

class MyTile{
  String title;
  List<MyTile> children=new List();
  MyTile(this.title);
//  MyTile(this.title,[this.children = const <MyTile>[]]);
}