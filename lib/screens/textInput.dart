import 'package:flutter/material.dart';
import 'package:ui_backup/components/chuTextFormField.dart';

class TextInputEx extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(child: ChuTextFormField(color: "#FEDF62",icon: Icons.perm_identity,hint: "ID",)),
    );
  }
}
