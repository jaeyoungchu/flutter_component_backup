class Contact{
  int id;
  String name,phone;

  Contact(this.id, this.name, this.phone);

  Contact.fromMap(Map<String, dynamic> map) {
    print(map);
    this.id = map['id'];
    this.name = map['name'];
    this.phone = map['phone'];
  }
}