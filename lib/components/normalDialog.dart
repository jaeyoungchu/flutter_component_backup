import 'package:flutter/material.dart';

Future<bool> chuNormalDialog(context, String detail){
  return showDialog(
      context: context,
      barrierDismissible: true,
      builder: (BuildContext context){
        return AlertDialog(
          title: Text('Detail'),
          content: Text(detail),
          actions: <Widget>[
            FlatButton(
              child: Text('Okay'),
              onPressed: (){
                Navigator.of(context).pop();
              },
            ),
          ],
        );
      }
  );
}
