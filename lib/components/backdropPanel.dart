import 'package:flutter/material.dart';
import 'dart:convert';
import 'package:http/http.dart' as http;


class BackdropPanels extends StatefulWidget {

  final AnimationController controller;

  BackdropPanels({this.controller});

  @override
  _BackdropPanelsState createState() => _BackdropPanelsState();
}

class _BackdropPanelsState extends State<BackdropPanels> {

  static const header_height = 32.0;

  List<User> users;
  User currentUser;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    _getUser();
  }

  Animation<RelativeRect> getPanelAnimation(BoxConstraints constraints){
    final height = constraints.biggest.height;
    final openedHeight = height - header_height;
    final closedHeight = -header_height;

    return RelativeRectTween(
        begin: RelativeRect.fromLTRB(0.0, 0.0, 0.0, 0.0),
        end: RelativeRect.fromLTRB(0.0, openedHeight, 0.0, closedHeight)
    ).animate(CurvedAnimation(parent: widget.controller, curve: Curves.linear));

//    return RelativeRectTween(
//        begin: RelativeRect.fromLTRB(0.0, openedHeight, 0.0, closedHeight),
//        end: RelativeRect.fromLTRB(0.0, 0.0, 0.0, 0.0)
//    ).animate(CurvedAnimation(parent: widget.controller, curve: Curves.linear));
  }

  Widget panels(BuildContext context, BoxConstraints constraints){
//    final ThemeData theme = Theme.of(context);
    return Container(
      child: Stack(
        children: <Widget>[
          Container(
//            color: Colors.orange[300],
            child: getListView(),
          ),
          PositionedTransition(
            rect: getPanelAnimation(constraints),
            child: Material(
              elevation: 12.0,
              borderRadius: BorderRadius.only(
                topLeft: Radius.circular(16.0),
                topRight: Radius.circular(16.0),
              ),
              child: Column(
                children: <Widget>[
                  Container(
                    height: header_height,
                    child: Center(
                      child: IgnorePointer(
                        ignoring: false,
                        child: ConstrainedBox(
                          constraints: BoxConstraints(minWidth: double.infinity),
                          child: RaisedButton(
                            shape: RoundedRectangleBorder(borderRadius: BorderRadius.only(topLeft: Radius.circular(16.0),
                              topRight: Radius.circular(16.0),)),
                            elevation: 1.0,
                            onPressed: (){flingPanel(-1);},
                            color: Colors.white,
                            highlightColor: Colors.orange[100],
                            child: Text("Shop here",style: TextStyle(color: Colors.black,fontSize: 20.0),),
                          ),
                        ),
                      ),
                    ),
                  ),
                  Expanded(
                    child: Container(
                      color: Colors.white,
                      child: Center(
                        child: currentUser == null ? SizedBox(width: 1.0,) : Image(image: NetworkImage(currentUser.picture)),
                      ),
                    ),
                  )
                ],
              ),
            ),
          ),
        ],
      ),
    );
  }

  void flingPanel(int currentIndex){
    widget.controller.fling(velocity:  isPanelVisible ? -1.0 : 1.0);
    if(currentIndex < 0){
      return;
    }
    setState(() {
      currentUser = users[currentIndex];
    });
  }

  @override
  Widget build(BuildContext context) {
    return LayoutBuilder(
      builder: panels,
    );
  }

  bool get isPanelVisible{
    final AnimationStatus status = widget.controller.status;
    return status == AnimationStatus.completed || status == AnimationStatus.forward;
  }

  Widget getListView() {
    if(users == null){
      return Scaffold(
        body: Center(child: CircularProgressIndicator(backgroundColor: Colors.orange,strokeWidth: 5.0,)),
      );
    }
    return Scaffold(
      body: ListView.builder(
        itemBuilder: (BuildContext context, int index) {
          return ListTile(
            key: PageStorageKey<User>(users[index]),
            onTap: (){setState(() {
              currentUser = users[index];
              flingPanel(index);
            });},
            leading: CircleAvatar(
              backgroundColor: Colors.white,
              backgroundImage: NetworkImage(users[index].picture.toString()),//snapshot.data[index].picture.toString()
//              backgroundImage: NetworkImage(_cutUri(
//                  "https://robohash.org/quibusdampraesentiumaccusantium.png")),
            ),
            title: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Text(
                  users[index].name,
                  style: TextStyle(fontSize: 25.0),
                ),
                Text(
                  "<${users[index].title}>",
                  style: TextStyle(fontSize: 12.0),
                ),
              ],
            ),
            subtitle: Text(users[index].email),
            contentPadding: EdgeInsets.all(12.0),
          );
        },
        itemCount: users.length,
      ),
    );
  }

  String _cutUri(String uri) {
    if (uri == null ||uri.isEmpty ) {
      return "https://robohash.org/quibusdampraesentiumaccusantium.png";
    }
    return uri.substring(0,uri.indexOf('?'));
  }

  void _getUser() async {
    var data = await http
        .get("https://api.mockaroo.com/api/df728080?count=10&key=726cf6b0");
    var jsonData = await json.decode(data.body);

    List<User> users = [];
    User user;
    for (var u in jsonData) {
      user = User(u["id"], u["title"], u["name"], u["email"], _cutUri(u["picture"]));
      users.add(user);
    }
    setState(() {
      this.users = users;
    });
  }
}


class User {
  final int id;
  final String title, name, email, picture;

  User(this.id, this.title, this.name, this.email, this.picture);
}