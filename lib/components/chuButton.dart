import 'package:flutter/material.dart';



class ChuButton extends StatelessWidget {

  final VoidCallback onPress;
  final String name;

  ChuButton(this.onPress,this.name);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 10.0),
      child: Container(
        height: 50.0,
        width: double.infinity,
        child: IgnorePointer(
          ignoring: false,
          child: RaisedButton(
            shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(25.0)),
            onPressed: onPress,
            color: Colors.blue,
            highlightColor: Colors.blueGrey,
            child: Text(name,style: TextStyle(color: Colors.white,fontSize: 20.0),),
          ),
        ),
      ),
    );
  }
}



//
//class ChuButton extends StatefulWidget {
//  @override
//  _ChuButtonState createState() => _ChuButtonState();
//}
//
//class _ChuButtonState extends State<ChuButton> with SingleTickerProviderStateMixin{
//
//  AnimationController sizeController;
//  Animation<double> sizeAnimation;
//  VoidCallback onButtonPressed;
//
//  @override
//  void initState() {
//    // TODO: implement initState
//    super.initState();
//    sizeController = AnimationController(duration:const Duration(milliseconds: 500),vsync: this);
//    sizeAnimation = Tween(begin: 20.0,end: 50.0).animate(sizeController)..addListener((){
//      setState(() {
//
//      });
//    });
//    sizeController.forward();
//  }
//
//  @override
//  Widget build(BuildContext context) {
//    return Container(
//      height: sizeAnimation.value,
//      padding: EdgeInsets.symmetric(horizontal: 20.0),
//      child: Material(
//        borderRadius: BorderRadius.circular(20.0),
//        shadowColor: Colors.orange[100],
//        color: Colors.deepOrange,
//        elevation: 7.0,
//        child: GestureDetector(
//          onTap: onButtonPressed,
//          child: Center(
//            child: Text("Button",style: TextStyle(color: Colors.white,fontWeight: FontWeight.bold,fontSize: 20.0),),
//          ),
//        ),
//      ),
//    );
//  }
//}
//
