import 'package:flutter/material.dart';
import 'package:ui_backup/utils/hexFromString.dart';


class ChuTextFormField extends StatelessWidget {
  final String hint,color;
  final IconData icon;


  ChuTextFormField({this.hint,this.icon,this.color});

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsets.only(left: 15.0, right: 15.0),
      child: Material(
        elevation: 5.0,
        borderRadius: BorderRadius.circular(5.0),
        child: TextFormField(
            decoration: InputDecoration(
                border: InputBorder.none,
                prefixIcon: Icon(icon,
                    color:
                    Color(hexFromString(color)),
                    size: 30.0),
                contentPadding:
                EdgeInsets.only(left: 15.0, top: 15.0),
                hintText: hint,
                hintStyle: TextStyle(
                    color: Colors.grey,
                    ))),
      ),
    );
  }
}
