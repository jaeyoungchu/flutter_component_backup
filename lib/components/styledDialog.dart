import 'package:flutter/material.dart';

Future<bool> chuStyledDialog(context,name)  {
  return showDialog(
      context: context,
    barrierDismissible: true,
    builder: (BuildContext context){
        return Dialog(
          shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(10)),
          child: Container(
            height: 250.0,
            width: 200.0,
            decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(20.0),
            ),
            child: Column(
              children: <Widget>[
                Stack(
                  children: <Widget>[
                    Container(
                      height: 100.0,
                    ),
                    Container(
                      height: 50.0,
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.only(topRight: Radius.circular(10.0),topLeft: Radius.circular(10.0)),
                        color: Colors.teal
                      ),
                    ),
                    Positioned(
                      top: 0.0,
                      left: 94.0,
                      child: Container(
                        height: 90.0,
                        width: 90.0,
                        decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(45.0),
                          border: Border.all(
                            color: Colors.green,
                            style: BorderStyle.solid,
                            width: 2.0
                          ),
                          image: DecorationImage(image: NetworkImage("https://robohash.org/quibusdampraesentiumaccusantium.png"),fit: BoxFit.cover),
                        ),
                      ),
                    ),
                  ],
                ),
                SizedBox(height: 20.0,),
                Padding(
                  padding: EdgeInsets.all(10.0),
                  child: Text(name,style: TextStyle(fontSize: 14.0,fontWeight: FontWeight.w300,),),
                ),
                SizedBox(height: 15.0,),
                FlatButton(
                  child: Center(
                    child: Text('Okay',style: TextStyle(fontSize: 14.0,color: Colors.teal),),
                  ),
                  onPressed: (){
                    Navigator.of(context).pop();
                  },
                  color: Colors.transparent,
                ),
              ],
            ),
          ),
        );
    }
  );
}
