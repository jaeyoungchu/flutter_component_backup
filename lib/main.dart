import 'package:flutter/material.dart';
import 'package:ui_backup/components/chuButton.dart';
import 'package:ui_backup/screens/backdropEx.dart';
import 'package:ui_backup/screens/chuButtonEx.dart';
import 'package:ui_backup/screens/dialogEx.dart';
import 'package:ui_backup/screens/expansionTileScreen.dart';
import 'package:ui_backup/screens/futureBuilderEx.dart';
import 'package:ui_backup/screens/multiListview.dart';
import 'package:ui_backup/screens/noFutureBuilderEx.dart';
import 'package:ui_backup/screens/progressButtonEx.dart';
import 'package:ui_backup/screens/sqfliteEx.dart';
import 'package:ui_backup/screens/textInput.dart';
import 'screens/home.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      routes: <String,WidgetBuilder>{
        '/chuTextInputEx' : (BuildContext context) => TextInputEx(),
        '/expansionTileEx' : (BuildContext context) => ExpansionTileEx(),
        '/futureBuilderEx' : (BuildContext context) => FutureBuilderEx(),
        '/noFutureBuilderEx' : (BuildContext context) => NoFutureBuilderEx(),
        '/chuButtonEx' : (BuildContext context) => AnimatedButtonEx(),
        '/progressButtonEx' : (BuildContext context) => ProgressButtonEx(),
        '/backdropEx' : (BuildContext context) => BackDropEx(),
        '/dialogEx' : (BuildContext context) => DialogEx(),
        '/multiListviewEx' : (BuildContext context) => MultiListViewEx(),
        '/sqfliteEx' : (BuildContext context) => SqfliteEx(),
      },
      title: 'Flutter Demo',
      theme: ThemeData(
        primarySwatch: Colors.orange,
      ),
      home: Home(),
    );
  }
}


